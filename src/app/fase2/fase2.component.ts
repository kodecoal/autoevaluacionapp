import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fase2',
  templateUrl: './fase2.component.html',
  styleUrls: ['./fase2.component.css']
})
export class Fase2Component implements OnInit {
  foods: Food[] = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
interface Food {
  value: string;
  viewValue: string;
}
