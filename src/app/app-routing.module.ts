import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// import de componentes
import { HomeComponent } from './home/home.component';
import { CrearProcesoComponent } from './fase1/crear-proceso/crear-proceso.component';
import { RegistroDeProgramasInstitucionalesComponent } from './fase1/registro-de-programas-institucionales/registro-de-programas-institucionales.component';
import { IntegrantesComiteComponent } from './fase1/integrantes-comite/integrantes-comite.component';
import { CreacionComiteComponent } from './fase1/creacion-comite/creacion-comite.component';
import { CronogramaComponent } from './fase1/cronograma/cronograma.component';
import { PonderacionComponent } from './fase1/ponderacion/ponderacion.component';
import { Fase2Component } from './fase2/fase2.component';
import { Fase3Component } from './fase3/fase3.component';
import { Fase4Component } from './fase4/fase4.component';
import { Fase5Component } from './fase5/fase5.component';


const routes: Routes = [
  { path:'',component:HomeComponent },
  { path:'home',component: HomeComponent },
  { path:'crearProceso',component:CrearProcesoComponent},
  { path:'registoDeProgramas',component:RegistroDeProgramasInstitucionalesComponent },
  { path:'integrantesComite',component:IntegrantesComiteComponent },
  { path:'creacionComite',component:CreacionComiteComponent },
  { path:'cronograma',component:CronogramaComponent },
  { path:'ponderacion',component:PonderacionComponent },
  { path:'fase2',component: Fase2Component },
  { path:'fase3',component: Fase3Component },
  { path:'fase4',component: Fase4Component },
  { path:'fase5',component: Fase5Component }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
