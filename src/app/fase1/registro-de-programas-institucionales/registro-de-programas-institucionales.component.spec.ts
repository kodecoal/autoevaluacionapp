import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroDeProgramasInstitucionalesComponent } from './registro-de-programas-institucionales.component';

describe('RegistroDeProgramasInstitucionalesComponent', () => {
  let component: RegistroDeProgramasInstitucionalesComponent;
  let fixture: ComponentFixture<RegistroDeProgramasInstitucionalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroDeProgramasInstitucionalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroDeProgramasInstitucionalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
