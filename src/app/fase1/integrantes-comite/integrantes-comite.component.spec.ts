import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntegrantesComiteComponent } from './integrantes-comite.component';

describe('IntegrantesComiteComponent', () => {
  let component: IntegrantesComiteComponent;
  let fixture: ComponentFixture<IntegrantesComiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntegrantesComiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrantesComiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
