import { Component, OnInit } from '@angular/core';
import { Factor, Characteristics} from '../../models/model.interface';
import { DataService } from "../../services/data.service";
@Component({
  selector: 'app-crear-proceso',
  templateUrl: './crear-proceso.component.html',
  styleUrls: ['./crear-proceso.component.css'],
  providers: [DataService]
})
export class CrearProcesoComponent implements OnInit {

  public selectedFactor: Factor = {id: 0, name:''}
  public factors: Factor[];
  public characteristics: Characteristics[];
  constructor(private dataSvc: DataService) { }

  ngOnInit() {
    this.factors = this.dataSvc.getFactors();
  }

  onSelect(id: number): void {
    this.characteristics = this.dataSvc.gerCharacteristic().filter(item=>item.factorId == id)
  }

}
