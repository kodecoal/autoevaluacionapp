import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

// Para servicios rest
import {HttpClientModule} from '@angular/common/http';

// componentes de AutoEvaluacionApp
import { LoginAutoEvaluacionAPPComponent } from './login-auto-evaluacion-app/login-auto-evaluacion-app.component';
import { HomeComponent } from './home/home.component';
import { RegistroDeProgramasInstitucionalesComponent } from './fase1/registro-de-programas-institucionales/registro-de-programas-institucionales.component';
import { CronogramaComponent } from './fase1/cronograma/cronograma.component';
import { PonderacionComponent } from './fase1/ponderacion/ponderacion.component';
import { CrearProcesoComponent } from './fase1/crear-proceso/crear-proceso.component';
import { CreacionComiteComponent } from './fase1/creacion-comite/creacion-comite.component';
import { IntegrantesComiteComponent } from './fase1/integrantes-comite/integrantes-comite.component';
import { Fase2Component } from './fase2/fase2.component';
import { Fase3Component } from './fase3/fase3.component';
import { Fase4Component } from './fase4/fase4.component';
import { Fase5Component } from './fase5/fase5.component';

// Material
import { MatToolbarModule, MatButtonModule, MatIconModule, MatListModule,MatSidenavModule } from '@angular/material';
import {MatTreeModule} from '@angular/material/tree';
import {MatMenuModule} from '@angular/material/menu';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatInputModule} from '@angular/material';

// ng-sidebar
import { SidebarModule } from 'ng-sidebar';




@NgModule({
  declarations: [
    AppComponent,
    LoginAutoEvaluacionAPPComponent,
    Fase2Component,
    Fase3Component,
    Fase4Component,
    Fase5Component,
    HomeComponent,
    RegistroDeProgramasInstitucionalesComponent,
    CronogramaComponent,
    PonderacionComponent,
    CrearProcesoComponent,
    CreacionComiteComponent,
    IntegrantesComiteComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    ReactiveFormsModule,
    MatTreeModule,
    FormsModule,
    MatMenuModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatSelectModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    SidebarModule.forRoot(),
    HttpClientModule


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
