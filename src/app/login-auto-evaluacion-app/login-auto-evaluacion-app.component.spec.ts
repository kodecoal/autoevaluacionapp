import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginAutoEvaluacionAPPComponent } from './login-auto-evaluacion-app.component';

describe('LoginAutoEvaluacionAPPComponent', () => {
  let component: LoginAutoEvaluacionAPPComponent;
  let fixture: ComponentFixture<LoginAutoEvaluacionAPPComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginAutoEvaluacionAPPComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginAutoEvaluacionAPPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
