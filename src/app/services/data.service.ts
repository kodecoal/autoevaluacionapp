import { Injectable } from '@angular/core';
import { Factor, Characteristics} from '../models/model.interface';

@Injectable(
  // {providedIn: 'root'}
  )
export class DataService {
  private factors : Factor[] = [
    {
      id:0,
      name:'--Seleccione un factor--'
    },
    {
      id: 1,
      name: 'FACTOR MISIÓN, PROYECTO INSTITUCIONAL Y DE PROGRAMA'
    },
    {
      id: 2,
      name: 'FACTOR ESTUDIANTES'
    },
    {
      id: 3,
      name: 'FACTOR PROFESORES'
    },
    {
      id: 4,
      name: 'FACTOR PROCESOS ACADÉMICOS'
    },
    {
      id: 5,
      name: 'FACTOR VISIBILIDAD NACIONAL E INTERNACIONAL'
    },
    {
      id: 6,
      name: 'FACTOR INVESTIGACIÓN, INNOVACIÓN Y CREACIÓN ARTÍSTICA Y CULTURAL'
    },
    {
      id: 7,
      name: 'FACTOR BIENESTAR INSTITUCIONAL'
    },
    {
      id: 8,
      name: 'FACTOR ORGANIZACIÓN, ADMINISTRACIÓN Y GESTIÓN'
    },
    {
      id: 9,
      name: 'FACTOR IMPACTO DE LOS EGRESADOS EN EL MEDIO'
    },
    {
      id: 10,
      name: 'FACTOR RECURSOS FÍSICOS Y FINANCIEROS'
    },

  ];
  private characteristics : Characteristics[] = [
    {
      id: 1,
      factorId: 1,
      name: 'Nº 1. Misión, Visión y Proyecto Institucional'
    },
    {
      id: 2,
      factorId: 1,
      name: 'Nº 2. Proyecto Educativo del Programa'
    },
    {
      id: 3,
      factorId: 1,
      name: 'Nº 3. Relevancia académica y pertinencia social del programa'
    },
    {
      id: 4,
      factorId: 2,
      name: 'Nº 4. Mecanismos de selección e ingreso'
    },
    {
      id: 5,
      factorId: 2,
      name: 'Nº 5. Estudiantes admitidos y capacidad institucional'
    },
    {
      id: 6,
      factorId: 2,
      name: 'Nº 6. Participación en actividades de formación integral'
    },
    {
      id: 7,
      factorId: 2,
      name: 'Nº 7. Reglamentos estudiantil y académico'
    },
    {
      id: 8,
      factorId: 3,
      name: 'Nº 8. Selección, vinculación y permanencia de profesores'
    },
    {
      id: 9,
      factorId: 3,
      name: 'Nº 9. Estatuto profesoral'
    },
    {
      id: 10,
      factorId: 3,
      name: 'Nº 10. Número, dedicación, nivel de formación y experiencia de los profesores'
    },
    {
      id: 11,
      factorId: 3,
      name: 'Nº 11. Desarrollo profesoral'
    },
    {
      id: 12,
      factorId: 3,
      name: 'Nº 12. Estímulos a la docencia, investigación, creación artística y cultural, extensión o proyección social y a la cooperación internacional'
    },
    {
      id: 13,
      factorId: 3,
      name: 'Nº 13. Producción, pertinencia, utilización e impacto de material docente'
    },
    {
      id: 14,
      factorId: 3,
      name: 'Nº 14. Remuneración por méritos'
    },
    {
      id: 15,
      factorId: 3,
      name: 'Nº 15. Evaluación de profesores'
    },
    {
      id: 16,
      factorId: 4,
      name: 'Nº 16. Integralidad del currículo'
    },
    {
      id: 17,
      factorId: 4,
      name: 'Nº 17. Flexibilidad del currículo'
    },
    {
      id: 18,
      factorId: 4,
      name: 'Nº 18. Interdisciplinariedad'
    },
    {
      id: 19,
      factorId: 4,
      name: 'Nº 19. Estrategias de enseñanza y aprendizaje'
    },
    {
      id: 20,
      factorId: 4,
      name: 'Nº 20. Sistema de evaluación de estudiantes'
    },
    {
      id: 21,
      factorId: 4,
      name: 'Nº 21. Trabajos de los estudiantes'
    },
    {
      id: 22,
      factorId: 4,
      name: 'Nº 22. Evaluación y autorregulación del programa'
    },
    {
      id: 23,
      factorId: 4,
      name: 'Nº 23. Extensión o proyección social'
    },
    {
      id: 24,
      factorId: 4,
      name: 'Nº 24. Recursos bibliográficos'
    },
    {
      id: 25,
      factorId: 4,
      name: 'Nº 25. Recursos informáticos y de comunicación'
    },
    {
      id: 26,
      factorId: 4,
      name: 'Nº 26. Recursos de apoyo docente'
    },
    {
      id: 27,
      factorId: 5,
      name: 'Nº 27. Inserción del programa en contextos académicos nacionales e internacionales'
    },
    {
      id: 28,
      factorId: 5,
      name: 'Nº 28. Relaciones externas de profesores y estudiantes'
    },
    {
      id: 29,
      factorId: 6,
      name: 'Nº 29. Formación para la investigación, la innovación y la creación artística y cultural'
    },
    {
      id: 30,
      factorId: 6,
      name: 'Nº 30. Compromiso con la investigación y la creación artística y cultural'
    },
    {
      id: 31,
      factorId: 7,
      name: 'Nº 31. Políticas, programas y servicios de bienestar universitario'
    },
    {
      id: 32,
      factorId: 7,
      name: 'Nº 32. Permanencia y retención estudiantil'
    },
    {
      id: 33,
      factorId: 8,
      name: 'Nº 33. Organización, administración y gestión del programa'
    },
    {
      id: 34,
      factorId: 8,
      name: 'Nº 34. Sistemas de comunicación e información'
    },
    {
      id: 35,
      factorId: 8,
      name: 'Nº 35. Dirección del programa'
    },
    {
      id: 36,
      factorId: 9,
      name: 'Nº 36. Seguimiento de los egresados'
    },
    {
      id: 37,
      factorId: 9,
      name: 'Nº 37. Impacto de los egresados en el medio social y académico'
    },
    {
      id: 38,
      factorId: 10,
      name: 'Nº 38. Recursos físicos'
    },
    {
      id: 39,
      factorId: 10,
      name: 'Nº 39. Presupuesto del programa'
    },
    {
      id: 40,
      factorId: 10,
      name: 'Nº 40. Administración de recursos'
    }
  ];

  getFactors(): Factor[]{
    return this.factors;
  }
  gerCharacteristic(): Characteristics[]{
    return this.characteristics;
  }




  constructor() { }
}
