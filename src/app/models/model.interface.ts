export interface Factor{
  id:number;
  name: string;
}

export interface Characteristics{
  id:number;
  factorId:number;
  name:string;
}
